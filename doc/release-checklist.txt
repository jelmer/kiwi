Check bugzilla
Go through the examples
  find examples/ -name \*.py|xargs -n1 python
Run pyflakes
Run all tests
Check buildbot
Update translations:
  bin/kiwi-i18n -pkiwi -u -c
Bump version number:
  kiwi/__version__.py
  doc/howto.tex
#  doc/release-announcement.tmpl
  kiwi.spec
---
Before you use make, read common/async.mk and Makefile
---
make docs
make release
make Changelog
  Add a release mark in debian/changelog (dch -v newversion)
Update NEWS
make deb
    Fix the lintian errors, the output is very verbose, so check the
    lines starting with an "E:" or "W:".
    Also, you might test the package to see if it's OK. In dist/ you
    will find the tarball and the .deb package.
Commit

make release-tag
make upload-release
  Ask Johan, he must do this
Add bugzilla version:
  Use your browser, wget won't work
  http://bugs/editversions.cgi?action=new&product=Kiwi&submit=Add&version=1.9.x
  Ask Kiko or Johan

Update website:
  make web
  Add new version to kiwi-www/src/getit.txt

Use release-anouncement.tmpl to write a new announcement
  Update Y in Subject
  Update X in Body
  Use the NEWS file to update the NEWS section of the announcement
